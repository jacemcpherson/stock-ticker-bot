IMAGE_NAME := stock-ticker-bot
SHELL := /bin/bash

install:
	pipx install poetry && poetry install

clean:
	rm -rf \
		.venv \
		dist \
		.mypy_cache \
		.ruff_cache

run:
	./setenv.sh python -m app.server

format:
	poetry run ruff format app

lint:
	poetry run ruff check && poetry run mypy .

lint-fix:
	poetry run ruff check --fix

build:
	docker build -t $(IMAGE_NAME) .

docker-run:
	docker run \
		--env-file .env \
		--env BOT_DB_LOCATION='/repo/database/database.sqlite' \
		--volume "./logs:/repo/logs" \
		--volume "./database:/repo/database" \
		$(IMAGE_NAME)
