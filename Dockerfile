FROM python:3.11-buster AS builder

RUN pip install poetry==1.8.3

ENV POETRY_NO_INTERACTION=1 \
  POETRY_VIRTUALENVS_IN_PROJECT=1 \
  POETRY_VIRTUALENVS_CREATE=1 \
  POETRY_CACHE_DIR=/tmp/poetry_cache

WORKDIR /repo

COPY pyproject.toml poetry.lock ./

RUN poetry install --without dev --no-root && rm -rf $POETRY_CACHE_DIR

FROM python:3.11-slim-buster AS runtime

WORKDIR /repo

ENV VIRTUAL_ENV=/repo/.venv \
  PATH=/repo/.venv/bin:$PATH

COPY --from=builder $VIRTUAL_ENV $VIRTUAL_ENV

ADD . /repo

CMD ["python", "-m", "app.server"]
