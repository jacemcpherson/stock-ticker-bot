#!/bin/bash

IMAGE_PATH="/tmp/xfer/$DOCKER_IMAGE_NAME.image.tar"
ENV_PATH="/tmp/xfer/.env"
HOME_ENV_PATH="$HOME/.env"
HOME_LOGS_PATH="$HOME/container-logs"
DATABASE_PATH="$HOME/database"

if [ ! -f "$IMAGE_PATH" ]; then
  echo "$IMAGE_PATH was not found"
  exit 1
fi

containers="$(docker ps -aq)"
echo "variable = $containers"
if [ -z "$containers" ]; then
  echo "No containers to stop / remove"
else
  echo "Removing containers ... $containers"
  docker stop $containers
  docker rm $containers
fi

echo "Pruning all images ..."
docker image prune -af

echo "Loading $IMAGE_PATH ..."
docker load --input "$IMAGE_PATH"

echo "Starting $DOCKER_IMAGE_NAME ..."
cp "$ENV_PATH" "$HOME_ENV_PATH"
docker run \
    --detach \
    --restart unless-stopped \
    --name "$DOCKER_IMAGE_NAME" \
    --env-file "$HOME_ENV_PATH" \
    --volume "$HOME_LOGS_PATH:/repo/logs" \
    --volume "$DATABASE_PATH:/repo/database" \
    "$DOCKER_IMAGE_NAME"

echo "Cleaning up xfer directory ..."
rm -rf "/tmp/xfer"
