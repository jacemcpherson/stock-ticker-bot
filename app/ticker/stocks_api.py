import abc
import enum
import math
from datetime import datetime
from typing import List, Optional, TypedDict

import pytz
import yfinance
from discord import Embed

from .util import get_mill_name

TIME_FORMAT = "%Y-%m-%d %H:%M"


class StockTickerDataPoint(TypedDict):
  x: str
  y: float


class StockDataRange(enum.Enum):
  ONE_DAY = "1d"
  FIVE_DAY = "5d"
  ONE_MONTH = "1mo"
  THREE_MONTH = "3mo"
  SIX_MONTH = "6mo"
  ONE_YEAR = "1y"

  def interval(self) -> str:
    if self == StockDataRange.ONE_DAY:
      return "15m"
    if self == StockDataRange.FIVE_DAY:
      return "30m"
    if (
      self == StockDataRange.ONE_MONTH
      or self == StockDataRange.THREE_MONTH
      or self == StockDataRange.SIX_MONTH
      or self == StockDataRange.ONE_YEAR
    ):
      return "1d"
    raise ValueError


class TickerDataType(enum.Enum):
  EQUITY = "EQUITY"
  ETF = "ETF"
  MUTUAL_FUND = "MUTUALFUND"

  def new_ticker_data(self) -> "TickerData":
    if self == TickerDataType.EQUITY or self == TickerDataType.ETF:
      return StockTickerData(self)
    if self == TickerDataType.MUTUAL_FUND:
      return MutualFundTickerData(self)
    raise ValueError

  def title_name(self) -> Optional[str]:
    if self == TickerDataType.EQUITY:
      return "Equity"
    if self == TickerDataType.ETF:
      return "ETF"
    if self == TickerDataType.MUTUAL_FUND:
      return "Mutual Fund"
    raise ValueError


class TickerData(abc.ABC):
  data_type: TickerDataType
  symbol: str
  long_name: str
  logo_url: str | None
  current_time: str
  market_cap: str | None
  dividend_yield: str | None
  pe_ratio: str | None
  previous_close: float
  year_high: float
  year_low: float
  _historical_data: List[StockTickerDataPoint]

  def __init__(self, data_type: TickerDataType) -> None:
    self.data_type = data_type

  @abc.abstractmethod
  def change_str(self) -> str: ...

  def parse_from_api(self, api_data: yfinance.Ticker) -> None:
    self.symbol = api_data.info["symbol"]
    self.long_name = api_data.info["longName"]
    self.logo_url = api_data.info.get("logo_url", None)
    self.current_time = get_current_time()
    self.market_cap = format_large_number(
      api_data.fast_info.market_cap or api_data.info.get("totalAssets"),
    )
    self.dividend_yield = (
      format_percentage(api_data.info["dividendYield"])
      if "dividendYield" in api_data.info
      else None
    )
    self.pe_ratio = (
      format_optional(api_data.info["trailingPE"])
      if "trailingPE" in api_data.info
      else None
    )
    self.previous_close = api_data.info["previousClose"]
    self.year_high = api_data.info["fiftyTwoWeekHigh"]
    self.year_low = api_data.info["fiftyTwoWeekLow"]

  @abc.abstractmethod
  def set_message_fields(self, message: Embed) -> None: ...

  def set_historical_data(self, historical_data: List[StockTickerDataPoint]) -> None:
    self._historical_data = historical_data

  def get_historical_data(self) -> List[StockTickerDataPoint]:
    return self._historical_data

  def get_preferred_data_range(self, input_range: StockDataRange) -> StockDataRange:
    return input_range


class StockTickerData(TickerData):
  current_price: float
  open: float
  high: float
  low: float

  def change_str(self) -> str:
    return format_change_percentage(self.open, self.current_price)

  def parse_from_api(self, api_data: yfinance.Ticker) -> None:
    super().parse_from_api(api_data)
    self.current_price = api_data.info["ask"]
    self.open = api_data.info["open"]
    self.high = api_data.info["dayHigh"]
    self.low = api_data.info["dayLow"]

  def set_message_fields(self, message: Embed) -> None:
    message.add_field(
      name="Current Price",
      value=f"{self.current_price}",
      inline=False,
    )
    message.add_field(name="Day Change", value=self.change_str(), inline=False)
    message.add_field(name="Open", value=f"{self.open}", inline=True)
    message.add_field(
      name="Market Cap",
      value=f"{self.market_cap if self.market_cap is not None else '--'}",
      inline=True,
    )
    message.add_field(
      name="Previous Close",
      value=f"{self.previous_close}",
      inline=True,
    )
    message.add_field(name="High", value=f"{self.high}", inline=True)
    message.add_field(
      name="PE ratio",
      value=f"{self.pe_ratio if self.pe_ratio is not None else '--'}",
      inline=True,
    )
    message.add_field(name="52-wk High", value=f"{self.year_high}", inline=True)
    message.add_field(name="Low", value=f"{self.low}", inline=True)
    message.add_field(
      name="Dividend Yield",
      value=f"{self.dividend_yield if self.dividend_yield is not None else '--'}",
      inline=True,
    )
    message.add_field(name="52-wk Low", value=f"{self.year_low}", inline=True)

  def set_historical_data(self, historical_data: List[StockTickerDataPoint]) -> None:
    self._historical_data = historical_data.copy()
    self._historical_data.append(
      {
        "x": self.current_time,
        "y": self.current_price,
      },
    )


class MutualFundTickerData(TickerData):
  ytd_return: str
  total_assets: str
  yield_ttm: str
  expense_ratio: str

  def parse_from_api(self, api_data: yfinance.Ticker) -> None:
    super().parse_from_api(api_data)
    self.ytd_return = format_percentage(api_data.info["ytdReturn"])
    self.total_assets = format_large_number(api_data.info["totalAssets"])
    self.yield_ttm = format_percentage(api_data.info["yield"])
    self.expense_ratio = format_percentage(
      api_data.info["annualReportExpenseRatio"],
    )

  def change_str(self) -> str:
    if len(self.get_historical_data()) < 2:
      return "0%"

    [point_1, point_2] = self._historical_data[-2:]
    prev_value = point_1["y"]
    current_value = point_2["y"]
    return format_change_percentage(prev_value, current_value)

  def current_price(self) -> str:
    if not self.get_historical_data():
      return "0.00"
    return format_to_places(self.get_historical_data()[-1]["y"], 2)

  def set_message_fields(self, message: Embed) -> None:
    message.add_field(
      name="Current Price",
      value=f"{self.current_price()}",
      inline=False,
    )
    message.add_field(name="Day Change", value=self.change_str(), inline=False)
    message.add_field(name="YTD return", value=f"{self.ytd_return}", inline=True)
    message.add_field(
      name="Expense ratio",
      value=f"{self.expense_ratio}",
      inline=True,
    )
    message.add_field(
      name="Total assets",
      value=f"{self.total_assets}",
      inline=True,
    )
    message.add_field(name="Yield (ttm)", value=f"{self.yield_ttm}", inline=True)
    message.add_field(name="52-wk High", value=f"{self.year_high}", inline=True)
    message.add_field(name="52-wk Low", value=f"{self.year_low}", inline=True)

  def set_historical_data(self, historical_data: List[StockTickerDataPoint]) -> None:
    self._historical_data = historical_data.copy()

  def get_preferred_data_range(self, input_range: StockDataRange) -> StockDataRange:
    if input_range == StockDataRange.ONE_DAY:
      return StockDataRange.ONE_MONTH
    return super().get_preferred_data_range(input_range)


def format_large_number(value: float | None) -> str:
  if not value:
    return "--"

  return get_mill_name(value)


def format_percentage(value: float | None) -> str:
  if value is None:
    return "--"
  return f"{value:.3%}"


def format_change_percentage(value_1: float, value_2: float) -> str:
  change_percentage = (value_2 - value_1) / value_1
  result = format_percentage(change_percentage)
  if not result.startswith("-"):
    return "+" + result
  return result


def format_to_places(value: float, places: int) -> str:
  return "{:.{}f}".format(value, places)


def format_optional(value: float) -> str:
  if value is None:
    return "--"
  return f"{value:.3}"


def get_current_time() -> str:
  return datetime.now(pytz.timezone("America/New_York")).strftime(TIME_FORMAT)


def get_historical_data(
  ticker: yfinance.Ticker,
  data_range: StockDataRange,
) -> List[StockTickerDataPoint]:
  data_frame = ticker.history(
    period=data_range.value,
    interval=data_range.interval(),
    prepost=True,
  )
  result: List[StockTickerDataPoint] = []

  keys = data_frame["Open"].index
  values = data_frame["Open"].values.tolist()

  for key, value in zip(keys, values):
    if math.isnan(value):
      continue
    result.append({"x": key.strftime(TIME_FORMAT), "y": value})

  return result


def get_ticker_data(symbol: str, data_range: StockDataRange) -> TickerData | None:
  try:
    stock_ticker = yfinance.Ticker(symbol)
    # Force fetch info, so we can see if this is a real symbol
    _ = stock_ticker.info

    ticker_data_type = TickerDataType(stock_ticker.info["quoteType"])
  except BaseException as e:  # noqa: BLE001
    raise Exception(f"Bruh are you dumb? I've never heard of {symbol}") from e

  ticker_data: TickerData = ticker_data_type.new_ticker_data()
  preferred_data_range = ticker_data.get_preferred_data_range(data_range)

  ticker_data.parse_from_api(stock_ticker)
  ticker_data.set_historical_data(
    get_historical_data(stock_ticker, preferred_data_range),
  )
  return ticker_data
