from quickchart import QuickChart

from .stocks_api import TickerData

CHART_WIDTH = 500
CHART_HEIGHT = 300
CHART_PIXEL_RATIO = 2.0

NO_DISPLAY = {"display": False}
QUICK_CHART_OPTIONS = {
  "legend": NO_DISPLAY,
  "scales": {
    "xAxes": [
      {
        "type": "time",
        "time": {"parser": "YYYY-MM-DD HH:mm"},
        "ticks": {"fontColor": "#fff"},
        "scaleLabel": NO_DISPLAY,
        "gridLines": NO_DISPLAY,
      },
    ],
    "yAxes": [{"ticks": {"fontColor": "#fff"}}],
  },
}


def is_negative(stock_ticker_data: TickerData) -> bool:
  earliest_point = min(stock_ticker_data.get_historical_data(), key=lambda x: x["x"])
  latest_point = max(stock_ticker_data.get_historical_data(), key=lambda x: x["x"])

  return earliest_point["y"] > latest_point["y"]


def get_chart_url(stock_ticker_data: TickerData) -> str:
  negative = is_negative(stock_ticker_data)

  quick_chart = QuickChart()
  quick_chart.width = CHART_WIDTH
  quick_chart.height = CHART_HEIGHT
  quick_chart.device_pixel_ratio = CHART_PIXEL_RATIO
  quick_chart.background_color = "transparent"
  quick_chart.config = {
    "type": "line",
    "data": {
      "datasets": [
        {
          "pointRadius": 0,
          "backgroundColor": "rgba(217, 48, 37, 0.25)"
          if negative
          else "rgba(52, 168, 83, 0.25)",
          "borderColor": "rgb(217, 48, 37)" if negative else "rgb(52, 168, 83)",
          "data": stock_ticker_data.get_historical_data(),
        },
      ],
    },
    "options": QUICK_CHART_OPTIONS,
  }

  return quick_chart.get_short_url()
