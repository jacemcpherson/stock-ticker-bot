import discord

from .chart_api import get_chart_url
from .commands import Command, CommandType, TickerCommand
from .stocks_api import get_ticker_data


def get_ticker_embed(
  bot_user: discord.ClientUser,
  command: TickerCommand,
) -> discord.Embed:
