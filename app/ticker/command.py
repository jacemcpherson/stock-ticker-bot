import asyncio
import logging
from concurrent.futures import ThreadPoolExecutor

import discord
from discord import app_commands

from app.common.command import try_or_followup_with_exception
from app.ticker.chart_api import get_chart_url
from app.ticker.stocks_api import StockDataRange, get_ticker_data


class TickerCommand(app_commands.Command):
  def __init__(self) -> None:
    super().__init__(
      name="ticker",
      description="Get stock ticker information",
      callback=self.ticker,
    )

    self.logger = logging.getLogger("TickerCommand")
    self.executor = ThreadPoolExecutor(max_workers=4)

  @app_commands.describe(ticker="the ticker name (stock symbol, mutual fund, etf)")
  @app_commands.describe(time_range="time range for historical data")
  async def ticker(
    self,
    interaction: discord.Interaction,
    ticker: str,
    time_range: StockDataRange = StockDataRange.ONE_DAY,
  ) -> None:
    await try_or_followup_with_exception(
      interaction,
      lambda: self._ticker(
        interaction=interaction,
        ticker=ticker,
        time_range=time_range,
      ),
    )

  async def _ticker(
    self,
    interaction: discord.Interaction,
    ticker: str,
    time_range: StockDataRange,
  ) -> None:
    if not interaction.guild:
      return
    if not interaction.channel:
      return

    await interaction.response.defer()
    embed = await asyncio.get_running_loop().run_in_executor(
      self.executor,
      self._get_embed,
      interaction,
      ticker,
      time_range,
    )

    if embed is None:
      await interaction.followup.send(
        f"Sorry bro, I couldn't get any data for {ticker}",
      )
      return

    await interaction.followup.send(embed=embed)

  def _get_embed(
    self,
    interaction: discord.Interaction,
    ticker: str,
    time_range: StockDataRange,
  ) -> discord.Embed | None:
    ticker_data = get_ticker_data(ticker, time_range)
    ticker_data = get_ticker_data(ticker, time_range)
    if ticker_data is None:
      return None

    embed = discord.Embed()
    embed.title = (
      f"{ticker_data.data_type.title_name()} "
      f"Summary > {ticker_data.long_name} ({ticker_data.symbol})"
    )

    if ticker_data.logo_url:
      embed.set_thumbnail(url=ticker_data.logo_url)
    else:
      assert interaction.client.user
      embed.set_thumbnail(url=interaction.client.user.display_avatar.url)

    ticker_data.set_message_fields(embed)

    embed.set_image(url=get_chart_url(ticker_data))
    return embed
