import logging

import discord
from discord import app_commands

from app.ticker.command import TickerCommand


class TickerBotClient(discord.Client):
  def __init__(self, ticker_command: TickerCommand) -> None:
    intents = discord.Intents.default()
    super().__init__(intents=intents)

    self.logger = logging.getLogger("TickerBotClient")

    self.tree = app_commands.CommandTree(self)
    self.tree.add_command(ticker_command)

  async def on_ready(self) -> None:
    self.logger.info(f"{self.user} is now connected")

    self.logger.info("Syncing Ticker bot commands...")
    await self.tree.sync()
    self.logger.info("Done syncing Ticker bot commands")

  # async def on_message(self, message: discord.Message) -> None:
  #   if message.author.bot:
  #     return

  #   bot_user = self.user
  #   if not bot_user:
  #     return

  #   try:
  #     command = commands.get_command(str(message.content))
  #     if command is None:
  #       return

  #     self.logger.debug(
  #       f"{message.author} asked for command: "
  #       f"{command.command_type} | args={command.args}",
  #     )
  #     response = messages.get_message_for_command(bot_user, command)
  #     await message.channel.send(embed=response)
  #   except Exception as e:  # noqa: BLE001
  #     logging.exception("There was an exception when handling the ticker command")
  #     await message.channel.send(str(e))
  #     return
