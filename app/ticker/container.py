import dependency_injector.containers as containers
import dependency_injector.providers as providers

import app.common.bots as bots
from app.ticker.command import TickerCommand

from .client import TickerBotClient


class TickerBotContainer(containers.DeclarativeContainer):
  api_token = providers.Dependency()
  disabled = providers.Dependency()

  ticker_command = providers.Singleton(TickerCommand)
  client = providers.Singleton(TickerBotClient, ticker_command=ticker_command)
  bot_client = providers.Singleton(
    bots.BotClient,
    client=client,
    api_token=api_token,
    disabled=disabled,
  )
