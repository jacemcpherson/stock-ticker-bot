mill_names = ["", "K", "M", "B", "T"]


def get_mill_name(value: float) -> str:
  mill_idx = 0
  while value > 1000 and mill_idx < len(mill_names) - 1:
    value /= 1000
    mill_idx += 1

  return f"{value:.2f}{mill_names[mill_idx]}"
