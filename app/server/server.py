import asyncio

import app.common.bots as bots


class Server:
  def __init__(
    self,
    manager_bot_client: bots.BotClient,
    ticker_bot_client: bots.BotClient,
    wealth_wall_bot_client: bots.BotClient,
  ) -> None:
    self.manager_bot_client = manager_bot_client
    self.ticker_bot_client = ticker_bot_client
    self.wealth_wall_bot_client = wealth_wall_bot_client

  async def start(self) -> None:
    await asyncio.gather(
      self.manager_bot_client.start(),
      self.ticker_bot_client.start(),
      self.wealth_wall_bot_client.start(),
    )

    print("Done")
