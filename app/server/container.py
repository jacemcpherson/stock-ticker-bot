import logging.config

import dependency_injector.containers as containers
import dependency_injector.providers as providers

import app.common.config as common_config
import app.manager.container
import app.server.server as server
import app.ticker.container
import app.wealth_wall.container
from app.common.connection import DbConnectionProvider


def database_configuration(env_prefix: str) -> providers.Configuration:
  config = providers.Configuration()

  config.database_location.from_env(f"{env_prefix}LOCATION", required=True)

  return config


class CoreContainer(containers.DeclarativeContainer):
  config = providers.Configuration(yaml_files=["config/logging.yaml"])
  logging = providers.Resource(logging.config.dictConfig, config)


class ApplicationContainer(containers.DeclarativeContainer):
  config = providers.Configuration()
  bot_database_config_vars = database_configuration(env_prefix="BOT_DB_")

  core = providers.Container(CoreContainer)

  manager_bot = providers.Container(
    app.manager.container.ManagerBotContainer,
    api_token=config.manager_bot_token,
    disabled=config.manager_bot_disabled,
  )

  ticker_bot = providers.Container(
    app.ticker.container.TickerBotContainer,
    api_token=config.ticker_bot_token,
    disabled=config.ticker_bot_disabled,
  )

  bot_db_config = providers.Singleton(
    common_config.DatabaseConfig,
    database_location=bot_database_config_vars.database_location,
  )

  bot_db_connection_provider = providers.Singleton(
    DbConnectionProvider,
    config=bot_db_config,
  )

  wealth_wall_bot = providers.Container(
    app.wealth_wall.container.WealthWallBotContainer,
    api_token=config.wealth_wall_bot_token,
    disabled=config.wealth_wall_bot_disabled,
    db_connection_provider=bot_db_connection_provider,
  )

  server = providers.Singleton(
    server.Server,
    manager_bot_client=manager_bot.bot_client,
    ticker_bot_client=ticker_bot.bot_client,
    wealth_wall_bot_client=wealth_wall_bot.bot_client,
  )
