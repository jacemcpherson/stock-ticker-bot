import asyncio

from dependency_injector.wiring import Provide, inject

from .container import ApplicationContainer
from .server import Server


@inject
async def main(server: Server = Provide[ApplicationContainer.server]) -> None:
  await server.start()


if __name__ == "__main__":
  container = ApplicationContainer()
  container.core.init_resources()
  container.config.manager_bot_token.from_env("MANAGER_BOT_TOKEN", required=True)
  container.config.manager_bot_disabled.from_env(
    "MANAGER_BOT_DISABLED",
    default=False,
    as_=bool,
  )
  container.config.ticker_bot_token.from_env("TICKER_BOT_TOKEN", required=True)
  container.config.ticker_bot_disabled.from_env(
    "TICKER_BOT_DISABLED",
    default=False,
    as_=bool,
  )

  container.config.wealth_wall_bot_token.from_env(
    "WEALTH_WALL_BOT_TOKEN",
    required=True,
  )
  container.config.wealth_wall_bot_disabled.from_env(
    "WEALTH_WALL_BOT_DISABLED",
    default=False,
    as_=bool,
  )

  container.wire(modules=[__name__])

  asyncio.run(main())
