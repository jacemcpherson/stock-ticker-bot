import datetime
import logging
from asyncio import Lock
from collections import defaultdict
from typing import TypedDict

import discord
import pytz
from discord import RawReactionActionEvent, app_commands

from app.common.emojis import get_emoji, get_emoji_name
from app.wealth_wall.command import setup
from app.wealth_wall.model import ChannelConfig, WallMessage, WallReaction
from app.wealth_wall.repository import WealthWallRepository

EASTERN = pytz.timezone("US/Eastern")
UTC = pytz.utc


def formatted_timestamp(timestamp: datetime.datetime) -> str:
  utc_timestamp = timestamp.replace(tzinfo=UTC)
  et_timestamp = utc_timestamp.astimezone(EASTERN)

  month = et_timestamp.strftime("%b")
  day = et_timestamp.strftime("%d").lstrip("0")
  year = et_timestamp.strftime("%Y")

  hour = et_timestamp.strftime("%I").lstrip("0")
  minute = et_timestamp.strftime("%M")
  ampm_zone = et_timestamp.strftime("%p %Z")

  return f"{month} {day}, {year} @ {hour}:{minute} {ampm_zone}"


class DiscordWallMessage(TypedDict):
  content: str
  embed: discord.Embed


class WealthWallBotClient(discord.Client):
  def __init__(
    self,
    setup_command: setup.SetupCommand,
    wealth_wall_repository: WealthWallRepository,
  ) -> None:
    intents = discord.Intents.all()
    super().__init__(intents=intents)

    self.logger = logging.getLogger("WealthWallBotClient")

    self.tree = app_commands.CommandTree(self)
    self.tree.add_command(setup_command)

    self.wealth_wall_repository = wealth_wall_repository
    self.post_locks: dict[int, Lock] = defaultdict(Lock)

  async def on_ready(self) -> None:
    self.logger.info(f"{self.user} is now connected")

    self.logger.info("Syncing WealthWall bot commands...")
    await self.tree.sync()
    self.logger.info("Done syncing WealthWall bot commands")

    self.wealth_wall_repository.initialize_cache()

  async def on_raw_reaction_add(self, payload: RawReactionActionEvent) -> None:
    if payload.user_id == self.user.id:
      return

    guild_id = payload.guild_id
    config = self.wealth_wall_repository.get_config(guild_id)

    emoji_name = get_emoji_name(payload.emoji.name) or payload.emoji.name
    if config.emoji != emoji_name:
      return

    wall_message = self.get_targeted_wall_message(config, payload)
    wall_message_id = self.wealth_wall_repository.upsert_wall_message(wall_message)

    wall_reaction = WallReaction(
      id=0,
      wall_message_id=wall_message_id,
      user_id=payload.user_id,
      message_id=payload.message_id,
    )
    self.wealth_wall_repository.upsert_wall_reaction(wall_reaction)

    await self.update_for_reactions(config, wall_message.id)

  async def on_raw_reaction_remove(self, payload: RawReactionActionEvent) -> None:
    if payload.user_id == self.user.id:
      return

    guild_id = payload.guild_id
    config = self.wealth_wall_repository.get_config(guild_id)

    emoji_name = get_emoji_name(payload.emoji.name) or payload.emoji.name
    if config.emoji != emoji_name:
      return

    wall_message = self.get_targeted_wall_message(config, payload)
    self.wealth_wall_repository.remove_reaction(
      wall_message.guild_id,
      wall_message.channel_id,
      wall_message.message_id,
      payload.message_id,
      payload.user_id,
    )

    if wall_message.id != 0:
      await self.update_for_reactions(config, wall_message.id)

  async def update_for_reactions(
    self,
    config: ChannelConfig,
    wall_message_id: int,
  ) -> None:
    async with self.post_locks[wall_message_id]:
      reaction_count = self.wealth_wall_repository.get_reaction_count(
        wall_message_id,
      )
      wall_message = self.wealth_wall_repository.get_wall_message_by_id(
        wall_message_id,
      )

      is_promoted = wall_message.wall_message_id is not None
      should_promote = reaction_count >= config.reactions
      if should_promote and not is_promoted:
        await self.promote(config, wall_message, reaction_count)
      elif should_promote:
        await self.update_wall_message(config, wall_message, reaction_count)
      elif not should_promote and is_promoted:
        await self.demote(config, wall_message)

  async def promote(
    self,
    config: ChannelConfig,
    wall_message: WallMessage,
    reactions: int,
  ) -> None:
    self.logger.info(f"Promoting with {reactions} reactions: {wall_message}")
    guild = self.get_guild(wall_message.guild_id)
    wall_channel = guild.get_channel(config.channel_id)
    message_channel = guild.get_channel(wall_message.channel_id)
    original_message = await message_channel.fetch_message(wall_message.message_id)
    original_author = original_message.author

    emoji = get_emoji(config.emoji)

    sent_message = await wall_channel.send(
      **await self.create_message(
        author=original_author,
        emoji=emoji,
        reactions=reactions,
        channel=message_channel,
        message=original_message,
      ),
    )
    wall_message.wall_message_id = sent_message.id
    self.wealth_wall_repository.upsert_wall_message(wall_message)

    await sent_message.add_reaction(emoji)

  async def update_wall_message(
    self,
    config: ChannelConfig,
    wall_message: WallMessage,
    reactions: int,
  ) -> None:
    self.logger.info(f"Updating to {reactions} reactions: {wall_message}")
    guild = self.get_guild(wall_message.guild_id)
    wall_channel = guild.get_channel(config.channel_id)
    original_channel = guild.get_channel(wall_message.channel_id)
    message_to_edit = await wall_channel.fetch_message(wall_message.wall_message_id)

    original_message = await original_channel.fetch_message(wall_message.message_id)
    original_author = original_message.author

    emoji = get_emoji(config.emoji)

    await message_to_edit.edit(
      **await self.create_message(
        author=original_author,
        emoji=emoji,
        reactions=reactions,
        channel=original_channel,
        message=original_message,
      ),
    )

  async def demote(self, config: ChannelConfig, wall_message: WallMessage) -> None:
    self.logger.info(f"Demoting: {wall_message}")
    guild = self.get_guild(wall_message.guild_id)
    assert guild
    wall_channel = guild.get_channel(config.channel_id)
    message_to_delete = await wall_channel.fetch_message(
      wall_message.wall_message_id,
    )
    await message_to_delete.delete()

    wall_message.wall_message_id = None
    self.wealth_wall_repository.upsert_wall_message(wall_message)

  def get_targeted_wall_message(
    self,
    config: ChannelConfig,
    payload: RawReactionActionEvent,
  ) -> WallMessage:
    wall_message = (
      self.wealth_wall_repository.get_wall_message_by_wall_message_id(
        wall_message_id=payload.message_id,
      )
      if payload.channel_id == config.channel_id
      else None
    )

    if wall_message is None:
      wall_message = self.wealth_wall_repository.get_wall_message(
        payload.guild_id,
        payload.channel_id,
        payload.message_id,
      )

    return wall_message or WallMessage(
      id=0,
      guild_id=payload.guild_id,
      channel_id=payload.channel_id,
      message_id=payload.message_id,
      wall_message_id=None,
      created_at=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
    )

  async def create_message(
    self,
    author: discord.Member,
    emoji: str,
    reactions: int,
    channel: discord.TextChannel,
    message: discord.Message,
  ) -> DiscordWallMessage:
    embed = discord.Embed()
    embed.set_author(
      name=author.display_name,
      icon_url=author.display_avatar.url,
    )
    embed.description = message.content
    if message.attachments:
      embed.set_image(url=message.attachments[0].url)
    if message.reference:
      replied_message = await channel.fetch_message(message.reference.message_id)
      if replied_message:
        embed.add_field(
          name=(
            f"{get_emoji('arrow_right_hook')} "
            f"Replying to {replied_message.author.display_name}"
          ),
          value=replied_message.content,
          inline=False,
        )
    embed.add_field(
      name="",
      value=f"[Jump to message]({message.jump_url})",
      inline=False,
    )
    embed.set_footer(text=formatted_timestamp(message.created_at))

    return {
      "content": (f"{emoji} **{reactions}** | {author.mention} | {channel.jump_url}"),
      "embed": embed,
    }
