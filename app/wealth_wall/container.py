import dependency_injector.containers as containers
import dependency_injector.providers as providers

import app.common.bots as bots
from app.common.connection import DbConnectionProvider
from app.wealth_wall.client import WealthWallBotClient
from app.wealth_wall.command.setup import SetupCommand
from app.wealth_wall.repository import WealthWallRepository


class WealthWallBotContainer(containers.DeclarativeContainer):
  api_token = providers.Dependency(instance_of=str)
  disabled = providers.Dependency(instance_of=bool)
  db_connection_provider = providers.Dependency(instance_of=DbConnectionProvider)

  wealth_wall_repository = providers.Singleton(
    WealthWallRepository,
    db_connection_provider=db_connection_provider,
  )

  setup_command = providers.Singleton(SetupCommand, repository=wealth_wall_repository)

  client = providers.Singleton(
    WealthWallBotClient,
    setup_command=setup_command,
    wealth_wall_repository=wealth_wall_repository,
  )
  bot_client = providers.Singleton(
    bots.BotClient,
    client=client,
    api_token=api_token,
    disabled=disabled,
  )
