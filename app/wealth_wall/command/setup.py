import logging

import discord
from discord import app_commands

from app.common.command import try_or_followup_with_exception
from app.common.emojis import get_emoji, get_emoji_name
from app.wealth_wall import WEALTH_WALL_ROLE
from app.wealth_wall.model import ChannelConfig
from app.wealth_wall.repository import WealthWallRepository


async def validate_emoji(guild: discord.Guild, emoji_str: str) -> str | None:
  if emoji_name := get_emoji_name(emoji_str):
    return emoji_name

  for emoji in guild.emojis:
    if str(emoji) == emoji_str:
      return emoji_str

  return None


class SetupCommand(app_commands.Command):
  def __init__(self, repository: WealthWallRepository) -> None:
    super().__init__(
      name="setup",
      description="Setup the WealthWall in this channel",
      callback=self.setup,
    )
    self.repository = repository

    self.logger = logging.getLogger("SetupCommand")

  @app_commands.checks.has_role(WEALTH_WALL_ROLE)
  @app_commands.describe(emoji="the emoji to use for the scoreboard")
  @app_commands.describe(
    reactions="the number of reactions needed to make it to the scoreboard",
  )
  async def setup(
    self,
    interaction: discord.Interaction,
    emoji: str,
    reactions: int,
  ) -> None:
    await try_or_followup_with_exception(
      interaction,
      lambda: self._setup(
        interaction=interaction,
        emoji=emoji,
        reactions=reactions,
      ),
      "Encountered exception while setting up WealthWall",
    )

  async def _setup(
    self,
    interaction: discord.Interaction,
    emoji: str,
    reactions: int,
  ) -> None:
    if not interaction.guild:
      return
    if not interaction.channel:
      return

    guild = interaction.guild
    channel = interaction.channel

    if reactions <= 0:
      await interaction.response.send_message("`reactions` must be positive")
      return

    validated_emoji = await validate_emoji(guild, emoji)
    if validated_emoji is None:
      await interaction.response.send_message(f"Unknown emoji: {emoji}")
      return

    self.logger.debug(
      f"Setting {guild} ({guild.id}), {channel} ({channel.id}), "
      f"emoji={validated_emoji}",
    )
    self.repository.upsert_config(
      ChannelConfig(
        guild_id=guild.id,
        channel_id=channel.id,
        emoji=validated_emoji,
        reactions=reactions,
      ),
    )
    await interaction.response.send_message(
      f"Set WealthWall to display in this channel with {reactions} "
      f"{get_emoji(validated_emoji)} reactions",
    )
