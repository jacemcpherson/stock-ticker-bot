import sqlite3
from contextlib import contextmanager
from typing import Generator

from app.common.connection import DbConnectionProvider
from app.common.sql import (
  insert_statement,
  select_all_statement,
  select_by_id_statement,
  select_by_statement,
  update_statement,
)
from app.wealth_wall.model import ChannelConfig, WallMessage, WallReaction


class WealthWallRepository:
  def __init__(self, db_connection_provider: DbConnectionProvider) -> None:
    self.db_connection_provider = db_connection_provider
    self.config_cache: dict[int, ChannelConfig] = {}

  @contextmanager
  def cursor(self) -> Generator[sqlite3.Cursor, None, None]:
    with self.db_connection_provider.cursor() as cursor:
      yield cursor

  def initialize_cache(self) -> None:
    configs = self.get_all_config()
    self.config_cache = {c.guild_id: c for c in configs}

  def upsert_config(self, config: ChannelConfig) -> None:
    existing_config = self.get_config(config.guild_id)

    with self.cursor() as cursor:
      if existing_config:
        cursor.execute(*update_statement(config))
      else:
        cursor.execute(*insert_statement(config))
    self.config_cache[config.guild_id] = config

  def get_all_config(self) -> list[ChannelConfig]:
    with self.cursor() as cursor:
      cursor.execute(*select_all_statement(ChannelConfig))
      rows = cursor.fetchall()
      return [ChannelConfig.from_row(row) for row in rows]

  def get_config(self, guild_id: int, skip_cache: bool = False) -> ChannelConfig | None:
    if not skip_cache and guild_id in self.config_cache:
      return self.config_cache[guild_id]

    with self.cursor() as cursor:
      cursor.execute(*select_by_id_statement(ChannelConfig, guild_id))
      rows = cursor.fetchall()
      if not rows:
        return None
      return ChannelConfig.from_row(rows[0])

  def upsert_wall_message(self, wall_message: WallMessage) -> int:
    """
    :return: the ID of the upserted wall message
    """
    with self.cursor() as cursor:
      if wall_message.id != 0:
        wall_message_id = wall_message.id
        cursor.execute(*update_statement(wall_message))
      elif existing_message := self.get_wall_message(
        wall_message.guild_id,
        wall_message.channel_id,
        wall_message.message_id,
      ):
        wall_message_id = existing_message.id
        wall_message.id = wall_message_id
        cursor.execute(*update_statement(wall_message))
      else:
        cursor.execute(*insert_statement(wall_message))
        wall_message_id = cursor.lastrowid or 0
        assert wall_message_id
        wall_message.id = wall_message_id

    return wall_message_id

  def get_wall_message_by_id(self, wall_message_id: int) -> WallMessage | None:
    with self.cursor() as cursor:
      cursor.execute(*select_by_id_statement(WallMessage, wall_message_id))
      rows = cursor.fetchall()
      if not rows:
        return None
      return WallMessage.from_row(rows[0])

  def get_wall_message_by_wall_message_id(
    self,
    wall_message_id: int,
  ) -> WallMessage | None:
    with self.cursor() as cursor:
      cursor.execute(
        *select_by_statement(WallMessage, {"wall_message_id": wall_message_id}),
      )
      rows = cursor.fetchall()
      if not rows:
        return None
      return WallMessage.from_row(rows[0])

  def get_wall_message(
    self,
    guild_id: int,
    channel_id: int,
    message_id: int,
  ) -> WallMessage | None:
    with self.cursor() as cursor:
      cursor.execute(
        *select_by_statement(
          WallMessage,
          {
            "guild_id": guild_id,
            "channel_id": channel_id,
            "message_id": message_id,
          },
        ),
      )
      rows = cursor.fetchall()
      if not rows:
        return None
      return WallMessage.from_row(rows[0])

  def upsert_wall_reaction(self, wall_reaction: WallReaction) -> int:
    """
    :return: the ID of the upserted wall reaction
    """
    with self.cursor() as cursor:
      if wall_reaction.id != 0:
        wall_reaction_id = wall_reaction.id
        cursor.execute(*update_statement(wall_reaction))
      elif existing_reaction := self.get_wall_reaction(
        wall_reaction.wall_message_id,
        wall_reaction.user_id,
        wall_reaction.message_id,
      ):
        wall_reaction_id = existing_reaction.id
        wall_reaction.id = wall_reaction_id
        cursor.execute(*update_statement(wall_reaction))
      else:
        cursor.execute(*insert_statement(wall_reaction))
        wall_reaction_id = cursor.lastrowid or 0
        assert wall_reaction_id
        wall_reaction.id = wall_reaction_id
    return wall_reaction_id

  def remove_reaction(
    self,
    guild_id: int,
    channel_id: int,
    message_id: int,
    reacted_message_id: int,
    user_id: int,
  ) -> None:
    with self.cursor() as cursor:
      cursor.execute(
        """
DELETE FROM wealth_wall_reactions
WHERE id IN (
    SELECT wr.id
    FROM wealth_wall_reactions wr
    INNER JOIN wealth_wall_messages wwm ON wr.wall_message_id = wwm.id
    WHERE wwm.guild_id = :guild_id
      AND wwm.channel_id = :channel_id
      AND wwm.message_id = :message_id
      AND wr.user_id = :user_id
      AND wr.message_id = :reacted_message_id
);
""",
        {
          "guild_id": guild_id,
          "channel_id": channel_id,
          "message_id": message_id,
          "reacted_message_id": reacted_message_id,
          "user_id": user_id,
        },
      )

  def get_wall_reaction(
    self,
    wall_message_id: int,
    user_id: int,
    message_id: int,
  ) -> WallReaction | None:
    with self.cursor() as cursor:
      cursor.execute(
        *select_by_statement(
          WallReaction,
          {
            "wall_message_id": wall_message_id,
            "user_id": user_id,
            "message_id": message_id,
          },
        ),
      )
      rows = cursor.fetchall()
      if not rows:
        return None
      return WallReaction.from_row(rows[0])

  def get_reaction_count(self, wall_message_id: int) -> int:
    with self.cursor() as cursor:
      cursor.execute(
        f"SELECT count(distinct user_id) "
        f"FROM {WallReaction.TABLE_NAME} WHERE "
        f"wall_message_id = :wall_message_id;",
        {
          "wall_message_id": wall_message_id,
        },
      )
      count_row = cursor.fetchone()
      assert count_row
      return count_row[0]
