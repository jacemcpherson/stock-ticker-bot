import dataclasses
import logging.handlers
import re
from pathlib import Path
from typing import Any, TypedDict


class AutoMkdirRotatingFileHandler(logging.handlers.RotatingFileHandler):
  def __init__(
    self,
    filename: str,
    mode: str = "a",
    maxBytes: int = 0,
    backupCount: int = 0,
    encoding: Any = None,
    delay: bool = False,
    errors: Any = None,
  ) -> None:
    Path(filename).parent.mkdir(parents=True, exist_ok=True)
    super().__init__(filename, mode, maxBytes, backupCount, encoding, delay, errors)


class NameFilterOverrideDict(TypedDict):
  name: str
  level: str


@dataclasses.dataclass
class Override:
  name: str
  levelno: int


class NameFilter(logging.Filter):
  def __init__(
    self,
    name: str = "",
    overrides: list[NameFilterOverrideDict] = None,
  ) -> None:
    super().__init__(name)
    levels_map = logging.getLevelNamesMapping()
    self.overrides = [
      Override(name=o["name"], levelno=levels_map[o["level"].upper()])
      for o in (overrides or [])
    ]

  def filter(self, record: logging.LogRecord) -> bool:
    for override in self.overrides:
      if re.match(override.name, record.name):
        return record.levelno >= override.levelno
    return True
