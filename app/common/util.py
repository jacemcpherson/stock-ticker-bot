from typing import AsyncIterator, Iterable, TypeVar

T = TypeVar("T")


async def aiter(iterable: Iterable[T]) -> AsyncIterator[T]:
  for i in iterable:
    yield i
