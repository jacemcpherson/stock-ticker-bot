import logging
import sys
import traceback
from typing import Any, Awaitable, Callable

import discord

logger = logging.getLogger(__name__)


async def try_or_followup_with_exception(
  interaction: discord.Interaction,
  func: Callable[[], Awaitable[Any]],
  exception_message: str = "Encountered exception while running command",
) -> None:
  try:
    await func()
  except BaseException as exception:
    logger.exception(exception_message)
    trace = "\n".join(traceback.format_exception(*sys.exc_info()))
    await interaction.response.send_message(
      content=f"""\
    {exception_message}:
    ```
    {exception}
    {trace}
    ```
    """,
      ephemeral=True,
    )
