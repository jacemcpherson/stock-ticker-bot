import logging
import sqlite3
from contextlib import contextmanager
from pathlib import Path
from typing import Any, Generator

from app.common.config import DatabaseConfig

_SCHEMA_PATH = Path(__file__).parent / "schema.sql"


class DbConnectionProvider:
  def __init__(self, config: DatabaseConfig) -> None:
    self._config = config
    self._connection: sqlite3.Connection | None = None

  @contextmanager
  def cursor(self) -> Generator[sqlite3.Cursor, Any, Any]:
    if self._connection is None:
      self._connection = self._init_connection()

    connection = self._connection
    try:
      cursor = connection.cursor()
      yield cursor
      connection.commit()
    except Exception as e:
      connection.rollback()
      raise e
    finally:
      cursor.close()

  def _init_connection(self) -> sqlite3.Connection:
    db_path = Path(self._config.database_location)
    init_schema = not db_path.exists()
    if not db_path.parent.exists():
      logging.warning(f"Creating new database folder at {db_path.parent.absolute()}")
      db_path.parent.mkdir(parents=True)

    cxn = sqlite3.connect(self._config.database_location)
    cxn.set_trace_callback(print)

    if init_schema:
      logging.warning(
        f"Creating new database at {db_path.absolute()} with "
        f"schema {_SCHEMA_PATH.absolute()}",
      )
      cursor = cxn.cursor()
      self._init_schema(cursor)
      cxn.commit()
      cursor.close()

    return cxn

  def _init_schema(self, cursor: sqlite3.Cursor) -> None:
    if not _SCHEMA_PATH.exists():
      logging.error(f"Tried to init schema, but schema.sql not found at {_SCHEMA_PATH}")
      return

    schema_text = _SCHEMA_PATH.read_text()
    cursor.executescript(schema_text)
