CREATE TABLE `cache_config` (
  `guild_id` bigint NOT NULL,
  `last_cached_at` text NOT NULL DEFAULT '2000-01-01 00:00:00',
  PRIMARY KEY (`guild_id`)
);
CREATE TABLE user_messages (
  id INTEGER PRIMARY KEY,
  guild_id INTEGER NOT NULL,
  channel_id INTEGER NOT NULL,
  message_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  created_at TEXT NOT NULL,
  deleted INTEGER NOT NULL DEFAULT 0
);
CREATE UNIQUE INDEX guild_channel_message_uniq ON user_messages (guild_id, channel_id, message_id);
CREATE INDEX user_id_idx ON user_messages (user_id);
CREATE TABLE wealth_wall_config (
  guild_id INTEGER NOT NULL,
  channel_id INTEGER,
  emoji TEXT NOT NULL,
  reactions INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (guild_id)
);
CREATE TABLE wealth_wall_messages (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  guild_id INTEGER NOT NULL,
  channel_id INTEGER NOT NULL,
  message_id INTEGER NOT NULL,
  wall_message_id INTEGER,
  created_at TEXT DEFAULT (datetime('now')),  -- SQLite uses a different syntax for current timestamp
  UNIQUE (guild_id, channel_id, message_id),
  UNIQUE (guild_id, wall_message_id)
);
CREATE TABLE wealth_wall_reactions (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  wall_message_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  message_id INTEGER NOT NULL,
  reacted_at TEXT DEFAULT (datetime('now')),  -- Using SQLite function for current timestamp
  UNIQUE (wall_message_id, user_id, message_id)
);
