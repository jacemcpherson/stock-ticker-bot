import abc
import collections
from typing import Any, ClassVar, Generic, Type, TypeVar

T = TypeVar("T")


def from_row(model_type: Type[T], row: tuple, columns: list[str]) -> T:
  return model_type(**{column: row[i] for i, column in enumerate(columns)})


PreparedStatement = collections.namedtuple("PreparedStatement", ("operation", "params"))


class SqlModel(abc.ABC, Generic[T]):
  TABLE_NAME: ClassVar[str]
  PRIMARY_KEY: ClassVar[str]
  AUTOGENERATED: ClassVar[bool]
  COLUMNS: ClassVar[list[str]]

  @staticmethod
  @abc.abstractmethod
  def from_row(row: tuple) -> T: ...


def separated(names: list[str]) -> str:
  return ", ".join(names)


def model_as_params(model: SqlModel) -> dict[str, Any]:
  return {column_name: getattr(model, column_name) for column_name in model.COLUMNS}


def select_all_statement(model_type: Type[SqlModel]) -> PreparedStatement:
  statement = PreparedStatement(
    operation=(f"SELECT {separated(model_type.COLUMNS)} FROM {model_type.TABLE_NAME};"),
    params={},
  )
  print(statement)
  return statement


def select_by_id_statement(
  model_type: Type[SqlModel],
  id: Any,  # noqa: A002
) -> PreparedStatement:
  statement = PreparedStatement(
    operation=(
      f"SELECT {separated(model_type.COLUMNS)} FROM {model_type.TABLE_NAME} "
      f"WHERE {model_type.PRIMARY_KEY} = :{model_type.PRIMARY_KEY};"
    ),
    params={model_type.PRIMARY_KEY: id},
  )
  print(statement)
  return statement


def select_by_statement(
  model_type: Type[SqlModel],
  params: dict[str, Any],
) -> PreparedStatement:
  conditions = [f"{column_name} = :{column_name}" for column_name in params]
  joined_conditions = " AND ".join(conditions)
  statement = PreparedStatement(
    operation=(
      f"SELECT {separated(model_type.COLUMNS)} FROM {model_type.TABLE_NAME} "
      f"WHERE {joined_conditions}"
    ),
    params=params,
  )
  print(statement)
  return statement


def insert_statement(model: SqlModel) -> PreparedStatement:
  columns = (
    model.COLUMNS
    if not model.AUTOGENERATED
    else [c for c in model.COLUMNS if c != model.PRIMARY_KEY]
  )

  value_params = [f":{column_name}" for column_name in columns]
  statement = PreparedStatement(
    operation=(
      f"INSERT INTO {model.TABLE_NAME} ({separated(columns)}) "
      f"VALUES ({separated(value_params)});"
    ),
    params=model_as_params(model),
  )
  print(statement)
  return statement


def update_statement(model: SqlModel) -> PreparedStatement:
  setters = [
    f"{column_name} = :{column_name}"
    for column_name in model.COLUMNS
    if column_name != model.PRIMARY_KEY
  ]
  statement = PreparedStatement(
    operation=(
      f"UPDATE {model.TABLE_NAME} SET {separated(setters)} "
      f"WHERE {model.PRIMARY_KEY} = :{model.PRIMARY_KEY};"
    ),
    params=model_as_params(model),
  )
  print(statement)
  return statement
