import logging

import discord


class BotClient:
  def __init__(
    self,
    client: discord.Client,
    api_token: str,
    disabled: bool = False,
  ) -> None:
    self.client = client
    self.api_token = api_token
    self.disabled = disabled

  async def start(self) -> None:
    if self.disabled:
      return None
    try:
      return await self.client.start(self.api_token)
    except BaseException:
      logging.exception("ERROR")
