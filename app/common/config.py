import dataclasses


@dataclasses.dataclass
class DatabaseConfig:
  database_location: str
