import discord
import discord.ui


class ConfirmView(discord.ui.View):
  def __init__(self) -> None:
    super().__init__()
    self.confirmed: bool | None = None
    self.timeout = 24 * 60 * 60

  @discord.ui.button(label="Continue", style=discord.ButtonStyle.red)
  async def confirm(
    self,
    interaction: discord.Interaction,  # noqa: ARG002
    button: discord.ui.Button,  # noqa: ARG002
  ) -> None:
    self.confirmed = True
    self.stop()

  @discord.ui.button(label="Cancel", style=discord.ButtonStyle.grey)
  async def cancel(
    self,
    interaction: discord.Interaction,  # noqa: ARG002
    button: discord.ui.Button,  # noqa: ARG002
  ) -> None:
    self.confirmed = False
    self.stop()
