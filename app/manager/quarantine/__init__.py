import hashlib
from typing import Optional

import discord

GATEKEEPER_ROLE = "Gatekeeper"
QUARANTINED_ROLE = "Quarantined"


def _unique_hash(member_id: int) -> str:
  user_id = f"{member_id}"
  computed_hash = hashlib.sha1(user_id.encode("utf-8")).digest().hex()  # noqa: S324
  return computed_hash[:10]


def quarantine_channel_name(member: discord.Member) -> str:
  """
  A channel name that will always be stable for the same Member
  """
  return f"quarantine-{_unique_hash(member.id)}"


def deleted_channel_name(member: discord.Member) -> str:
  return f"deleted-{_unique_hash(member.id)}"


def get_role_with_name(guild: discord.Guild, name: str) -> Optional[discord.Role]:
  for role in guild.roles:
    if role.name == name:
      return role
  return None


def get_channel_with_name(
  guild: discord.Guild,
  name: str,
) -> Optional[discord.TextChannel]:
  for channel in guild.text_channels:
    if name.lower() in channel.name.lower():
      return channel
  return None


def get_category_with_name(
  guild: discord.Guild,
  name: str,
) -> Optional[discord.CategoryChannel]:
  for category in guild.categories:
    if name.lower() in category.name.lower():
      return category
  return None


def get_quarantined_user(
  channel: discord.TextChannel,
  role: discord.Role,
) -> discord.Member | None:
  for member in channel.members:
    if role not in member.roles:
      continue
    channel_name = quarantine_channel_name(member)
    if channel.name == channel_name:
      return member
  return None
