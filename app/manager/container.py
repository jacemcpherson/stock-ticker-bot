import dependency_injector.containers as containers
import dependency_injector.providers as providers

import app.common.bots as bots

from .client import ManagerBotClient
from .command.cleanup import CleanupCommand
from .command.deny import DenyCommand
from .command.grant import GrantCommand
from .handler.quarantine import QuarantineHandler


class ManagerBotContainer(containers.DeclarativeContainer):
  config = providers.Configuration()

  api_token = providers.Dependency()
  disabled = providers.Dependency()

  deny_command = providers.Singleton(DenyCommand)
  cleanup_command = providers.Singleton(CleanupCommand)
  quarantine_handler = providers.Singleton(QuarantineHandler)
  grant_command = providers.Singleton(
    GrantCommand,
    quarantine_handler=quarantine_handler,
  )

  client = providers.Singleton(
    ManagerBotClient,
    grant_command=grant_command,
    deny_command=deny_command,
    cleanup_command=cleanup_command,
    quarantine_handler=quarantine_handler,
  )
  bot_client = providers.Singleton(
    bots.BotClient,
    client=client,
    api_token=api_token,
    disabled=disabled,
  )
