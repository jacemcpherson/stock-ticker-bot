import asyncio
import datetime
from typing import Awaitable, Callable, Literal, Union

import discord
import pytz

from app.manager import quarantine

_Action = Union[Literal["grant"], Literal["leave"]]


async def _run_twice(pause_duration: float, func: Callable[..., Awaitable]) -> None:
  await asyncio.sleep(pause_duration)
  await func()
  await asyncio.sleep(pause_duration)
  await func()


class QuarantineHandler:
  async def on_member_join(self, member: discord.Member) -> None:
    guild = member.guild
    quarantine_role = quarantine.get_role_with_name(
      guild,
      quarantine.QUARANTINED_ROLE,
    )
    await member.add_roles(quarantine_role)

    quarantine_category = quarantine.get_category_with_name(
      guild,
      name="quarantine",
    )
    if quarantine_category is None:
      raise ValueError("Server must be configured with a 'Quarantine' category")

    channel_name = quarantine.quarantine_channel_name(member)
    channel = await quarantine_category.create_text_channel(name=channel_name)

    await _run_twice(
      1.5,
      lambda: channel.set_permissions(
        member,
        overwrite=discord.PermissionOverwrite(
          read_messages=True,
          send_messages=True,
        ),
      ),
    )

    await asyncio.sleep(1)
    await channel.send(
      f"""\
Hey {member.mention}, welcome to **{guild.name}**! To help us prevent spam, please:

* Share a short intro to let us know why you've joined the server
* A moderator will approve your access shortly after
""",
    )

  async def on_member_remove(self, member: discord.Member) -> None:
    member_roles = [role.name for role in member.roles]
    if quarantine.QUARANTINED_ROLE not in member_roles:
      # Do nothing, user wasn't quarantined
      return

    channel_name = await self.cleanup_quarantine_channel(member, action="leave")

    gatekeepers_channel = quarantine.get_channel_with_name(
      member.guild,
      "gatekeepers",
    )

    await gatekeepers_channel.send(
      f"`{member.name}` left while quarantined. Their quarantine channel "
      f"`{channel_name}` has been deleted",
    )

  async def cleanup_quarantine_channel(
    self,
    member: discord.Member,
    action: _Action,
  ) -> str:
    guild = member.guild
    channel_name = quarantine.quarantine_channel_name(member)
    quarantine_channel = quarantine.get_channel_with_name(guild, channel_name)

    if not quarantine_channel:
      return channel_name

    await self._send_conversation_log(member, quarantine_channel, action)

    await quarantine_channel.edit(name=quarantine.deleted_channel_name(member))
    await asyncio.sleep(2.0)
    await quarantine_channel.delete(
      reason=f"`{member.name}` left server while quarantined",
    )

    return channel_name

  async def _send_conversation_log(
    self,
    member: discord.Member,
    quarantine_channel: discord.TextChannel,
    action: _Action,
  ) -> None:
    messages = [
      m async for m in quarantine_channel.history(oldest_first=True) if not m.author.bot
    ]
    if not messages:
      return

    timestamp = datetime.datetime.utcnow().astimezone(
      tz=pytz.timezone("America/New_York"),
    )
    embed = discord.Embed(
      title=f"{member.name} chat log ({timestamp:%Y-%m-%d %I:%M:%S %p %Z})",
      description=f"_From #{quarantine_channel.name}_",
      color=discord.Color.green() if action == "grant" else discord.Color.red(),
    )

    for message in messages:
      message_content = str(message.content) or ""
      if message.stickers:
        message_content += "\n<sticker>" if message_content else "<sticker>"
      embed.add_field(
        name=message.author.name,
        value=message_content[:1024],
        inline=False,
      )

    await quarantine.get_channel_with_name(member.guild, "gatekeepers").send(
      embed=embed,
    )
