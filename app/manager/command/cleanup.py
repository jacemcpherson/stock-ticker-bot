import discord.utils
from discord import app_commands

from app.common.command import try_or_followup_with_exception
from app.manager.quarantine import (
  GATEKEEPER_ROLE,
  QUARANTINED_ROLE,
  get_channel_with_name,
  get_quarantined_user,
  get_role_with_name,
)


class CleanupCommand(app_commands.Command):
  def __init__(self) -> None:
    super().__init__(
      name="cleanup",
      description="Kick a quarantined user from the server",
      callback=self.cleanup,
    )

  @app_commands.checks.has_role(GATEKEEPER_ROLE)
  async def cleanup(self, interaction: discord.Interaction) -> None:
    await try_or_followup_with_exception(
      interaction,
      lambda: self._cleanup(interaction=interaction),
      "Encountered exception while cleaning up",
    )

  async def _cleanup(self, interaction: discord.Interaction) -> None:
    if not interaction.guild:
      return
    if not interaction.channel:
      return

    guild = interaction.guild
    channel = interaction.channel
    if not channel.name.startswith("quarantine"):
      raise Exception(f"Invalid channel name: {channel.name}")

    quarantined_role = get_role_with_name(guild, QUARANTINED_ROLE)
    if not quarantined_role:
      raise Exception(f"Role '{QUARANTINED_ROLE}' not found")

    member = get_quarantined_user(channel, quarantined_role)
    await interaction.response.send_message(
      content=f"{interaction.user.name} cleaning up quarantine for {member.mention}",
    )

    await self.log_to_gatekeepers(guild, interaction.user, member)
    await member.kick(
      reason=f"{interaction.user.name} denied access to {member.name}",
    )

  async def log_to_gatekeepers(
    self,
    guild: discord.Guild,
    denier: discord.Member,
    user: discord.Member,
  ) -> None:
    gatekeepers_channel = get_channel_with_name(guild, "gatekeepers")
    if gatekeepers_channel is None:
      return
    await gatekeepers_channel.send(
      f"`{denier.name}` denied access to {user.mention} (they have been kicked)",
    )
