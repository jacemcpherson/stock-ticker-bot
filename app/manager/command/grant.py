import asyncio
import textwrap

import discord
import discord.utils
from discord import app_commands

from app.common.command import try_or_followup_with_exception
from app.manager.handler.quarantine import QuarantineHandler
from app.manager.quarantine import (
  GATEKEEPER_ROLE,
  QUARANTINED_ROLE,
  get_channel_with_name,
  get_quarantined_user,
  get_role_with_name,
)


class GrantCommand(app_commands.Command):
  def __init__(self, quarantine_handler: QuarantineHandler) -> None:
    super().__init__(
      name="grant",
      description=(
        "Grant a new user access to the server (by removing the 'Quarantined' " "role)"
      ),
      callback=self.grant,
    )
    self._quarantine_handler = quarantine_handler

  @app_commands.checks.has_role(GATEKEEPER_ROLE)
  async def grant(self, interaction: discord.Interaction) -> None:
    await try_or_followup_with_exception(
      interaction,
      lambda: self._grant(interaction=interaction),
      "Encountered exception while granting",
    )

  async def _grant(self, interaction: discord.Interaction) -> None:
    if not interaction.guild:
      return
    if not interaction.channel:
      return

    guild = interaction.guild
    channel = interaction.channel
    if not channel.name.startswith("quarantine"):
      raise Exception(f"Invalid channel name: {channel.name}")

    quarantined_role = get_role_with_name(guild, QUARANTINED_ROLE)
    if not quarantined_role:
      raise Exception(f"Role '{QUARANTINED_ROLE}' not found")

    member = get_quarantined_user(channel, quarantined_role)
    await member.remove_roles(
      quarantined_role,
      reason=f"{interaction.user.name} approved access",
    )
    await interaction.response.send_message(
      content=f"{interaction.user.name} approved access for {member.mention}",
    )

    # followup with a welcome mention in the #general channel, if there is one
    await self.send_welcome(guild, member)
    await self.log_to_gatekeepers(guild, interaction.user, member)

    await asyncio.sleep(2)
    await self._quarantine_handler.cleanup_quarantine_channel(
      member,
      action="grant",
    )

  async def send_welcome(self, guild: discord.Guild, user: discord.Member) -> None:
    general_channel = get_channel_with_name(guild, "general")
    intro_channel = get_channel_with_name(guild, "introductions")
    pf_channel = get_channel_with_name(guild, "personal-finance")
    party_parrot_emoji = discord.utils.get(guild.emojis, name="partyparrot") or ""

    if general_channel is None or intro_channel is None or pf_channel is None:
      # do nothing, server is not configured well for our welcome message
      return

    await general_channel.send(
      content=textwrap.dedent(
        f"""\
        Hey {user.mention}, welcome to the {guild.name} server!

        Say hello here, introduce yourself in {intro_channel.mention}, or ask \
questions in {pf_channel.mention}.

        Checkout out our server commands by running `!commands`.

        We're happy to have you! {party_parrot_emoji}
        """,
      ),
    )

  async def log_to_gatekeepers(
    self,
    guild: discord.Guild,
    approver: discord.Member,
    user: discord.Member,
  ) -> None:
    gatekeepers_channel = get_channel_with_name(guild, "gatekeepers")
    if gatekeepers_channel is None:
      return
    await gatekeepers_channel.send(
      f"`{approver.name}` granted access to {user.mention}",
    )
