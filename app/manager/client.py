import logging

import discord
import discord.ui
from discord import app_commands

from app.manager.command import cleanup, deny, grant
from app.manager.handler import quarantine


class ManagerBotClient(discord.Client):
  def __init__(
    self,
    grant_command: grant.GrantCommand,
    deny_command: deny.DenyCommand,
    cleanup_command: cleanup.CleanupCommand,
    quarantine_handler: quarantine.QuarantineHandler,
  ) -> None:
    intents = discord.Intents.all()
    super().__init__(intents=intents)

    self.logger = logging.getLogger("ManagerBotClient")

    self.tree = app_commands.CommandTree(self)
    self.tree.add_command(grant_command)
    self.tree.add_command(deny_command)
    self.tree.add_command(cleanup_command)

    self.quarantine_handler = quarantine_handler

  async def on_ready(self) -> None:
    self.logger.info(f"{self.user} is now connected")

    self.logger.info("Syncing Manager bot commands...")
    await self.tree.sync()
    self.logger.info("Done syncing Manager bot commands")

  async def on_member_join(self, member: discord.Member) -> None:
    await self.quarantine_handler.on_member_join(member)

  async def on_raw_member_remove(self, payload: discord.RawMemberRemoveEvent) -> None:
    await self.quarantine_handler.on_member_remove(payload.user)
